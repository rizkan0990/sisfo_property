<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin - Melati Bersama </title>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap-grid.css">

    <!-- Fontawesome -->
    <link rel="stylesheet" href="../assets/style/css/fontawesome.min.css">


</head>
<body>


<!-- ===================================================================================== -->
    <!-- Jquery -->
    <script src="../assets/style/js/jquery"></script>

    <!-- Bootstrap -->
    <script src="../assets/style/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="../assets/style/bootstrap/js/bootstrap.js"></script>
    <script src="../assets/style/bootstrap/js/bootstrap.popper.min.js"></script>
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>

    <!-- Fontawesome -->
    <script src="../assets/style/js/fontawesome.all.min.js"></script>

</body>
</html>